package net.thumbtack.kkadoshnikov.http.client.handlers;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.util.ReferenceCountUtil;
import net.thumbtack.kkadoshnikov.http.client.util.ChannelAttributes;
import net.thumbtack.kkadoshnikov.http.client.util.HttpResponse;
import net.thumbtack.kkadoshnikov.http.client.util.impl.HttpResponseImpl;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@ChannelHandler.Sharable
public class HttpResponseHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext channelHandlerContext, Object msg) {
        try {
            if (msg instanceof FullHttpResponse) {
                final FullHttpResponse nettyResponse = (FullHttpResponse) msg;
                int status = nettyResponse.getStatus().code();
                Map<String, String> headers = new HashMap<>();
                nettyResponse.headers().forEach((entry) -> headers.putIfAbsent(entry.getKey(), entry.getValue()));
                String body = nettyResponse.content().toString(Charset.defaultCharset());
                final HttpResponse httpResponse = new HttpResponseImpl(status, headers, body);
                channelHandlerContext.
                        channel().
                        attr(ChannelAttributes.HTTP_RESPONSE_FUTURE).
                        get().
                        complete(httpResponse);
            }
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
        ctx.channel().
                attr(ChannelAttributes.HTTP_RESPONSE_FUTURE).
                get().
                completeExceptionally(cause);
    }
}