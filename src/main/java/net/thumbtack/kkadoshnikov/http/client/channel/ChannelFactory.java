package net.thumbtack.kkadoshnikov.http.client.channel;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpContentDecompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import net.thumbtack.kkadoshnikov.http.client.handlers.HttpResponseHandler;
import net.thumbtack.kkadoshnikov.http.client.util.Constants;
import net.thumbtack.kkadoshnikov.http.client.util.impl.HttpRequestImpl;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class ChannelFactory {

    private final Bootstrap bootstrap;
    private final HttpResponseHandler responseHandler = new HttpResponseHandler();

    private final Map<String, ChannelPool> pools = new ConcurrentHashMap<>();

    public ChannelFactory() {
        bootstrap = new Bootstrap();

        bootstrap.group(new NioEventLoopGroup())
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {

                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new HttpClientCodec(Constants.MAX_INITIAL_LINE_LENGTH,
                                Constants.MAX_HEADER_SIZE, Constants.MAX_CHUNK_SIZE, false));
                        pipeline.addLast(new HttpContentDecompressor());
                        pipeline.addLast(new HttpObjectAggregator(Constants.MAX_CONTENT_LENGTH));
                        pipeline.addLast(responseHandler);
                    }
                });
    }

    public CompletableFuture<ChannelFuture> getChannel(HttpRequestImpl.URL url) {
        if (pools.containsKey(url.getName())) {
            return pools.get(url.getName()).getChannel();
        }
        return newPool(url).getChannel();
    }

    private ChannelPool newPool(HttpRequestImpl.URL url) {
        ChannelPool pool = new FixedChannelPool(() -> bootstrap.connect(url.getHost(), url.getPort()), url.getName());
        pools.put(url.getName(), pool);
        return pool;
    }

    public void releaseChannel(String host, ChannelFuture channel) {
        pools.get(host).releaseChannel(channel);
    }

    public void closeChannel(String host, ChannelFuture channel) {
        pools.get(host).closeChannel(channel);
    }
}

