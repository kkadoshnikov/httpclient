package net.thumbtack.kkadoshnikov.http.client.util;

import java.util.Map;
import java.util.Optional;

public interface HttpResponse {

    int getStatus();

    Map<String, String> getHeaders();

    Optional<String> getBody();
}
