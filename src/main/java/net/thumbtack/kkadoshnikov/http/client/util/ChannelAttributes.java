package net.thumbtack.kkadoshnikov.http.client.util;

import io.netty.util.AttributeKey;

import java.util.concurrent.CompletableFuture;

public class ChannelAttributes {
    public static final AttributeKey<CompletableFuture<HttpResponse>> HTTP_RESPONSE_FUTURE
            = AttributeKey.newInstance("http-response-future");
    public static final AttributeKey<Long> TIMESTAMP
            = AttributeKey.newInstance("timestamp");
    public static final AttributeKey<String> POOL_NAME
            = AttributeKey.newInstance("pool-name");
}
