package net.thumbtack.kkadoshnikov.http.client.util;

import com.codahale.metrics.*;

public class MetricUtil {

    private MetricUtil() {

    }

    private static MetricRegistry registry = new MetricRegistry();

    public enum Metric {
        EXCEPTIONS_TOTAL("exceptions.total"),
        TIMER("timer"),
        CONNECTIONS_ACTIVE("connections.active"),
        CONNECTIONS_FREE("connections.free");

        private final String s;

        Metric(String s) {
            this.s = s;
        }

        @Override
        public String toString() {
            return s;
        }
    }

    public static void registryGauge (String name, Metric type,  Gauge gauge) {
        registry.register(name + "." + type, gauge);
    }

    public static Timer getTimer(String name) {
        return registry.timer(name + "." + Metric.TIMER);
    }

    public static Meter getMeter(String name, Metric type) {
        return registry.meter(name + "." + type);
    }

    public static Meter getMeter(String name, Class<? extends Throwable> throwable) {
        return registry.meter(name + ".exceptions." + throwable.getSimpleName());
    }

    public static Gauge getGauge(String name, Metric type) {
        return registry.getGauges().get(name + "." + type);
    }

    public static void clear() {
        registry = new MetricRegistry();
    }
}
