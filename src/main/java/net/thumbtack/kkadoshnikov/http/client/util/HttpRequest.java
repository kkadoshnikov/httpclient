package net.thumbtack.kkadoshnikov.http.client.util;

import io.netty.handler.codec.http.HttpMethod;
import net.thumbtack.kkadoshnikov.http.client.util.impl.HttpRequestImpl;

import java.util.Map;

public interface HttpRequest {

    HttpMethod getMethod();

    String getBody();

    HttpRequestImpl.URL getURL();

    Map<String, String> getHeaders();

}
