package net.thumbtack.kkadoshnikov.http.client.util.impl;

import io.netty.handler.codec.http.HttpMethod;
import net.thumbtack.kkadoshnikov.http.client.util.HttpRequest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class HttpRequestImpl implements HttpRequest {

    static public class URL {
        private final String uri;
        private final String host;
        private final String scheme;
        private final String name;
        private final int port;

        public URL (String uri) throws URISyntaxException {
            this.uri = uri;
            URI URI = new URI(uri);
            host = URI.getHost() == null ? "127.0.0.1" : URI.getHost();
            scheme = URI.getScheme() == null ? "http" : URI.getScheme();
            name = scheme + "://" + host;
            if (URI.getPort() == -1) {
                if ("http".equalsIgnoreCase(scheme)) {
                    port = 80;
                } else if ("https".equalsIgnoreCase(scheme)) {
                    port = 443;
                } else {
                    port = -1;
                }
            } else {
                port = URI.getPort();
            }
        }

        public String getUri() {
            return uri;
        }

        public String getHost() {
            return host;
        }

        public String getScheme() {
            return scheme;
        }

        public String getName() {
            return name;
        }

        public int getPort() {
            return port;
        }
    }

    private final HttpMethod method;
    private final Map<String, String> headers;
    private final String body;
    private final URL URL;

    private HttpRequestImpl(HttpMethod method, String body, URL URL, Map<String, String> headers) {
        this.method = method;
        this.URL = URL;
        this.body = body;
        this.headers = headers;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public String getBody() {
        return body;
    }

    public URL getURL() {
        return URL;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }


    public static class Builder {
        private HttpMethod method;
        private final Map<String, String> headers = new HashMap<>();
        private String body;
        private URL URL;

        public Builder setMethod(HttpMethod method) {
            this.method = method;
            return this;
        }

        public Builder setURL(String URL) throws URISyntaxException {
            this.URL = new URL(URL);
            return this;
        }

        public Builder setURL(URL URL) {
            this.URL = URL;
            return this;
        }

        public Builder setBody(String body) {
            this.body = body;
            return this;
        }

        public Builder addHeaders(String... headers) {
            for (int i = 0; i < headers.length / 2; i++) {
                this.headers.put(headers[i * 2], headers[i * 2 + 1]);
            }
            return this;
        }

        public HttpRequestImpl create() {
            return new HttpRequestImpl(method, body, URL, headers);
        }
    }

}
