package net.thumbtack.kkadoshnikov.http.client.util.exceptions;

public class NettyHttpClientException extends RuntimeException {

    public NettyHttpClientException(Throwable cause) {
        super(cause);
    }

    public NettyHttpClientException(String message) {
        super(message);
    }
}
