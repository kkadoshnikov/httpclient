package net.thumbtack.kkadoshnikov.http.client.util.impl;

import net.thumbtack.kkadoshnikov.http.client.util.HttpResponse;

import java.util.*;

public class HttpResponseImpl implements HttpResponse {

    private final int status;
    private final Map<String, String> headers;
    private final Optional<String> body;

    public HttpResponseImpl(int status, Map<String, String> headers, String body) {
        this.status = status;
        this.headers = headers;
        if (body == null || body.isEmpty()) {
            this.body = Optional.empty();
        } else {
            this.body = Optional.of(body);
        }
    }

    public int getStatus() {
        return status;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Optional<String> getBody() {
        return body;
    }

}
