package net.thumbtack.kkadoshnikov.http.client.service;

import com.codahale.metrics.Timer;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.*;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;
import net.thumbtack.kkadoshnikov.http.client.channel.ChannelFactory;
import net.thumbtack.kkadoshnikov.http.client.util.ChannelAttributes;
import net.thumbtack.kkadoshnikov.http.client.util.HttpRequest;
import net.thumbtack.kkadoshnikov.http.client.util.HttpResponse;
import net.thumbtack.kkadoshnikov.http.client.util.MetricUtil;
import net.thumbtack.kkadoshnikov.http.client.util.exceptions.NettyHttpClientException;

import javax.net.ssl.SSLException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class NettyHttpClientService implements HttpClientService {

    private final ChannelFactory channelFactory = new ChannelFactory();

    public CompletableFuture<HttpResponse> execute(HttpRequest request) {
        try {
            CompletableFuture<HttpResponse> response = new CompletableFuture<>();
            supply(request).whenComplete(((httpResponse, throwable) -> {
                if (throwable == null) {
                    response.complete(httpResponse);
                }
                else {
                    response.completeExceptionally(throwable);
                }
            }));
            return response;
        } catch (InterruptedException | ExecutionException | SSLException e) {
            throw new NettyHttpClientException(e);
        }
    }

    private CompletableFuture<HttpResponse> supply(HttpRequest request) throws InterruptedException, ExecutionException, SSLException {
        ChannelFuture channelFuture;
        channelFuture = channelFactory.getChannel(request.getURL()).get();
        if (request.getURL().getScheme().equals("https")) {
            addSSlHandler(channelFuture);
        }
        try {
            return executeChannel(request, channelFuture);
        } catch (NettyHttpClientException e) {
            channelFactory.closeChannel(request.getURL().getName(), channelFuture);
            throw new NettyHttpClientException(e);
        }
    }

    private CompletableFuture<HttpResponse> executeChannel(HttpRequest request, final ChannelFuture channelFuture){
        CompletableFuture<HttpResponse> response = new CompletableFuture<>();
        channelFuture.channel().attr(ChannelAttributes.HTTP_RESPONSE_FUTURE).set(response);
        FullHttpRequest tmp = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, request.getMethod(), request.getURL().getUri());
        request.getHeaders().forEach(tmp.headers()::add);
        String poolName = channelFuture.channel().attr(ChannelAttributes.POOL_NAME).get();
        Timer.Context context = MetricUtil.getTimer(poolName).time();
        channelFuture.channel().writeAndFlush(tmp);
        response.whenComplete(((httpResponse, throwable) -> {
            context.stop();
            if (httpResponse.getHeaders().containsKey("Connection") && httpResponse.getHeaders().get("Connection").equals("close")) {
                channelFactory.closeChannel(request.getURL().getName(), channelFuture);
            } else {
                channelFactory.releaseChannel(request.getURL().getName(), channelFuture);
            }
            if (throwable != null) {
                MetricUtil.getMeter(poolName, MetricUtil.Metric.EXCEPTIONS_TOTAL).mark();
                MetricUtil.getMeter(poolName, throwable.getClass()).mark();
            }
        }));
        return response;
    }

    private void addSSlHandler(ChannelFuture channelFuture) throws SSLException {
        ChannelPipeline pipeline = channelFuture.channel().pipeline();
        if (pipeline.toMap().entrySet().iterator().next().getValue().getClass() != (SslHandler.class)) {
            pipeline.addFirst(SslContextBuilder.forClient().build().newHandler(channelFuture.channel().alloc()));
            channelFuture.syncUninterruptibly();
        }
    }
}
