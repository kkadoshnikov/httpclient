package net.thumbtack.kkadoshnikov.http.client.channel;

import io.netty.channel.ChannelFuture;

import java.util.concurrent.CompletableFuture;

public interface ChannelPool {

    CompletableFuture<ChannelFuture> getChannel();

    void releaseChannel(ChannelFuture channel);

    void closeChannel(ChannelFuture channel);

}
