package net.thumbtack.kkadoshnikov.http.client.channel;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import net.thumbtack.kkadoshnikov.http.client.util.ChannelAttributes;
import net.thumbtack.kkadoshnikov.http.client.util.Constants;
import net.thumbtack.kkadoshnikov.http.client.util.MetricUtil;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class FixedChannelPool implements ChannelPool {

    private final Supplier<ChannelFuture> supplier;
    private final BlockingDeque<ChannelFuture> channels = new LinkedBlockingDeque<>(Constants.FIXED_POOL_SIZE);
    private final String name;
    private AtomicInteger activeChannel = new AtomicInteger();

    public FixedChannelPool(Supplier<ChannelFuture> supplier, String name) {
        this.supplier = supplier;
        this.name = name;
        MetricUtil.registryGauge(name, MetricUtil.Metric.CONNECTIONS_FREE, channels::size);
        MetricUtil.registryGauge(name, MetricUtil.Metric.CONNECTIONS_ACTIVE, activeChannel::get);
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.schedule(this::clearTimeoutChannel, Constants.CHANNEL_CLEAR_DELAY, TimeUnit.MILLISECONDS);
    }

    private void clearTimeoutChannel() {
        while (!channels.isEmpty() && channels.peekFirst().channel().attr(ChannelAttributes.TIMESTAMP).get()
                + Constants.CHANNEL_IDLE_TIMEOUT < System.currentTimeMillis()) {
            ChannelFuture first = channels.pollFirst();
            if (first != null) {
                first.channel().close();
            }
        }
        channels.size();
    }

    public CompletableFuture<ChannelFuture> getChannel() {
        ChannelFuture channel = channels.pollLast();
        activeChannel.incrementAndGet();
        if (channel == null || !channel.channel().isActive()) {
            return createChannel();
        }
        CompletableFuture<ChannelFuture> result = new CompletableFuture<>();
        result.complete(channel);
        return result;
    }

    private CompletableFuture<ChannelFuture> createChannel() {
        CompletableFuture<ChannelFuture> result = new CompletableFuture<>();
        ChannelFuture channelFuture = supplier.get();
        channelFuture.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                result.complete(future);
            }
        });
        channelFuture.channel().attr(ChannelAttributes.POOL_NAME).set(name);
        return result;
    }

    public void releaseChannel(ChannelFuture channelFuture) {
        channelFuture.channel().attr(ChannelAttributes.TIMESTAMP).set(System.currentTimeMillis());
        channels.offer(channelFuture);
        activeChannel.decrementAndGet();
    }

    @Override
    public void closeChannel(ChannelFuture channelFuture) {
        channelFuture.channel().close();
        activeChannel.decrementAndGet();
    }
}
