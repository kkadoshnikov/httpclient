package net.thumbtack.kkadoshnikov.http.client.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Constants {
    public static final int FIXED_POOL_SIZE;
    public static final int CHANNEL_IDLE_TIMEOUT;
    public static final int CHANNEL_CLEAR_DELAY;
    public static final int MAX_CONTENT_LENGTH;
    public static final int MAX_INITIAL_LINE_LENGTH;
    public static final int MAX_HEADER_SIZE;
    public static final int MAX_CHUNK_SIZE;

    private Constants() {
    }

    static {
        Config conf = ConfigFactory.load();
        FIXED_POOL_SIZE = conf.getInt("httpClient.fixed_pool_size");
        CHANNEL_IDLE_TIMEOUT = conf.getInt("httpClient.channel_idle_timeout");
        CHANNEL_CLEAR_DELAY = conf.getInt("httpClient.channel_clear_delay");
        MAX_CONTENT_LENGTH = conf.getInt("httpClient.max_content_length");
        MAX_INITIAL_LINE_LENGTH = conf.getInt("httpClient.max_initial_line_length");
        MAX_HEADER_SIZE = conf.getInt("httpClient.max_header_size");
        MAX_CHUNK_SIZE = conf.getInt("httpClient.max_header_size");
    }

}
