package net.thumbtack.kkadoshnikov.http.client.service;

import net.thumbtack.kkadoshnikov.http.client.util.HttpRequest;
import net.thumbtack.kkadoshnikov.http.client.util.HttpResponse;

import java.util.concurrent.CompletableFuture;

public interface HttpClientService {

    /**
     * Performs asynchronous http request.
     *
     * @param request
     * @return future with delayed http response
     */
    CompletableFuture<HttpResponse> execute(HttpRequest request);

}
