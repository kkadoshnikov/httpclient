package net.thumbtack.kkadoshnikov.http.client;

import io.netty.handler.codec.http.HttpMethod;
import net.thumbtack.kkadoshnikov.http.client.service.HttpClientService;
import net.thumbtack.kkadoshnikov.http.client.service.NettyHttpClientService;
import net.thumbtack.kkadoshnikov.http.client.util.HttpRequest;
import net.thumbtack.kkadoshnikov.http.client.util.HttpResponse;
import net.thumbtack.kkadoshnikov.http.client.util.MetricUtil;
import net.thumbtack.kkadoshnikov.http.client.util.impl.HttpRequestImpl;
import org.junit.Assert;
import org.junit.Test;

import java.net.URISyntaxException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class HttpClientTest {

    @Test
    public void apacheTest() throws ExecutionException, InterruptedException, URISyntaxException {
        HttpRequest request = new HttpRequestImpl.Builder().setMethod(HttpMethod.GET).setURL("http://88.198.26.2:80/dist/").
                addHeaders("Host", "www.apache.org").create();
        HttpClientService service = new NettyHttpClientService();
        for (int i = 0; i < 5; i++) {
            CompletableFuture<HttpResponse> future = service.execute(request);
            int status = future.get().getStatus();
            Assert.assertEquals(200, status);
        }
        testMetrics(request);
        Assert.assertEquals(1, MetricUtil.getGauge(request.getURL().getName(), MetricUtil.Metric.CONNECTIONS_FREE).getValue());
        MetricUtil.clear();
    }

    @Test
    public void wikiTest() throws InterruptedException, ExecutionException, URISyntaxException {
        HttpRequest request = new HttpRequestImpl.Builder().setMethod(HttpMethod.GET).setURL("https://ru.wikipedia.org/wiki/HTTP").
                addHeaders("Host", "wikipedia.org", "User-Agent",
                        "Mozilla/5.0 (X11; U; Linux i686; ru; rv:1.9b5) Gecko/2008050509 Firefox/3.0b5",
                        "Accept", "text/html").create();
        HttpClientService service = new NettyHttpClientService();
        for (int i = 0; i < 5; i++) {
            CompletableFuture<HttpResponse> future = service.execute(request);
            int status = future.get().getStatus();
            Assert.assertEquals(200, status);
        }
        testMetrics(request);
        Assert.assertEquals(1, MetricUtil.getGauge(request.getURL().getName(), MetricUtil.Metric.CONNECTIONS_FREE).getValue());
        MetricUtil.clear();
    }

    @Test
    public void wikiConnectionCloseTest() throws InterruptedException, ExecutionException, URISyntaxException {
        HttpRequest request = new HttpRequestImpl.Builder().setMethod(HttpMethod.GET).setURL("https://ru.wikipedia.org/wiki/HTTP").
                addHeaders("Host", "ru.wikipedia.org",
                        "User-Agent", "Mozilla/5.0 (X11; U; Linux i686; ru; rv:1.9b5) Gecko/2008050509 Firefox/3.0b5",
                        "Accept", "text/html", "Connection", "close").create();
        HttpClientService service = new NettyHttpClientService();
        for (int i = 0; i < 5; i++) {
            CompletableFuture<HttpResponse> future = service.execute(request);
            int status = future.get().getStatus();
            Assert.assertEquals(200, status);
        }
        testMetrics(request);
        Assert.assertEquals(0, MetricUtil.getGauge(request.getURL().getName(), MetricUtil.Metric.CONNECTIONS_FREE).getValue());
        MetricUtil.clear();
    }

    private void testMetrics(HttpRequest request) {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(5, MetricUtil.getTimer(request.getURL().getName()).getCount());
        Assert.assertEquals(0, MetricUtil.getMeter(request.getURL().getName(), MetricUtil.Metric.EXCEPTIONS_TOTAL).getCount());
        Assert.assertEquals(0, MetricUtil.getMeter(request.getURL().getName(), TimeoutException.class).getCount());
        Assert.assertEquals(0, MetricUtil.getGauge(request.getURL().getName(), MetricUtil.Metric.CONNECTIONS_ACTIVE).getValue());
    }
}
